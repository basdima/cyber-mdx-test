import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class AuthService {
  private endpoint = 'http://34.73.153.218:8085/api/accounts/login/';


  constructor(private http: HttpClient) { }

  public auth(username: string, password: string): Observable<any> {
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };
    const body = new URLSearchParams();
    body.set('user', username);
    body.set('pass', password);
    return this.http.post(this.endpoint, body.toString(), options);
  }
}
