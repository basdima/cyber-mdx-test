import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {LoginComponent} from './views/login/login.component';
import {FormsModule} from '@angular/forms';
import {StatusComponent} from './views/status/status.component';
import {HttpClientModule} from '@angular/common/http';
import {AuthService} from './services/auth.service';
import {BlockComponent} from './components/block.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    StatusComponent,
    BlockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
