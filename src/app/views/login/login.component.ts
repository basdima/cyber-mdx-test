import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated,
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html'
})
export class LoginComponent {
  public username: string;
  public password: string;
  @HostBinding('class.is-loading') public loading = false;

  constructor(private authService: AuthService, private cd: ChangeDetectorRef, private router: Router) {
  }

  public signInButtonClickHandler() {
    this.loading = true;
    const req = this.authService.auth(this.username, this.password);
    req.subscribe(() => {
      this.router.navigate(['status']);
    }, () => {
      alert('Auth error');
      this.loading = false;
      this.cd.markForCheck();
    });
  }
}
