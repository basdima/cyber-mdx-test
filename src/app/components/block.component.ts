import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-block',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './block.component.html',
  styleUrls: ['./block.component.scss'],
})
export class BlockComponent {
  @Input() statusSuccessful: boolean;
  @Input() name: string;
  @Input() addr: string;
  @Input() logo: string;
}
